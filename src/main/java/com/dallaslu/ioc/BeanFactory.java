package com.dallaslu.ioc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.QName;
import org.dom4j.io.SAXReader;

public class BeanFactory implements BeanContext {
	private static final Log log = LogFactory.getLog(BeanFactory.class);

	private Map<String, Object> beanMap = new HashMap<String, Object>();

	private BeanFactory() {

	}

	private static class Holder {
		public static BeanFactory instance = new BeanFactory();
	}

	public static BeanFactory getInstance() {
		return Holder.instance;
	}

	public Object getBean(String id) {
		Object obj = beanMap.get(id);
		if (obj == null) {
			log.warn("bean is null: " + id);
		}
		return obj;
	}

	public BeanContext parse(String filePath) {
		InputStream in = null;
		try {
			in = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			log.error("File not found: " + filePath);
		}
		return parse(in);
	}

	/**
	 * parse configuration, initialize beans
	 * 
	 * @param in
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public BeanContext parse(InputStream in) {

		Document doc = readDocument(in);

		if (doc != null) {
			Iterator<Element> iBean = doc.getRootElement().elementIterator(
					"bean");
			// initialize beans
			initBeans(iBean);

			// set properties
			iBean = doc.getRootElement().elementIterator("bean");
			setAttrs(iBean);
		}

		return this;

	}

	@SuppressWarnings("unchecked")
	private void setAttrs(Iterator<Element> iBean) {
		Element be;
		while (iBean.hasNext()) {
			be = iBean.next();
			String id = be.attributeValue(new QName("id"));
			String beanName = be.attributeValue(new QName("name"));
			String type = be.attributeValue(new QName("class"));

			Object bean = getBean(id == null ? beanName : id);
			Iterator<Element> iProp = be.elementIterator("property");
			Element pe = null;

			while (iProp.hasNext()) {
				pe = iProp.next();
				String name = pe.attributeValue(new QName("name"));
				String ref = pe.attributeValue(new QName("ref"));
				String value = pe.attributeValue(new QName("value"));

				log.debug("property name: " + name);

				@SuppressWarnings("rawtypes")
				Class beanClass = null;
				try {
					beanClass = Class.forName(type);
					Method m = getSetter(beanClass, name);

					if (ref == null) {
						setValue(m, bean, value);
					} else {
						m.invoke(bean, getBean(ref));
					}

				} catch (ClassNotFoundException e) {
					log.error("Class Not Found: " + beanClass);
				} catch (Exception e) {
					log.error("failed setting property " + name
							+ " for instance of " + type + ": " + id, e);
				}

			}

		}
	}

	private void initBeans(Iterator<Element> iBean) {
		Element be;
		while (iBean.hasNext()) {
			be = iBean.next();
			String id = be.attributeValue(new QName("id"));
			String name = be.attributeValue(new QName("name"));
			String type = be.attributeValue(new QName("class"));
			Object bean = null;
			try {
				bean = Class.forName(type).newInstance();
				log.debug("created instance of " + type + ": " + id);
			} catch (InstantiationException e) {
				log.error("Exception", e);
			} catch (IllegalAccessException e) {
				log.error("Exception", e);
			} catch (ClassNotFoundException e) {
				log.error("ClassNotFound: " + type);
			} catch (Exception e) {
				log.error("failed to create instance of " + type + ": " + id, e);
			}
			if (id == null) {
				beanMap.put(name, bean);
			} else {
				beanMap.put(id, bean);
			}

		}
	}

	private Document readDocument(final InputStream in) {
		try {
			SAXReader reader = new SAXReader();
			return reader.read(in);

		} catch (Exception e) {
			log.error("error when read configuration", e);
			return null;
		}
	}

	private void setValue(Method m, Object bean, String value) throws Exception {
		@SuppressWarnings("rawtypes")
		Class paramType = m.getParameterTypes()[0];

		if (paramType == byte.class || paramType == Byte.class) {
			Byte b = new Byte(value);
			m.invoke(bean, b);
		} else if (paramType == short.class || paramType == Short.class) {
			Short s = new Short(value);
			m.invoke(bean, s);
		} else if (paramType == char.class || paramType == Character.class) {
			Character c = new Character(value.charAt(0));
			m.invoke(bean, c);
		} else if (paramType == int.class || paramType == Integer.class) {
			Integer i = new Integer(value);
			m.invoke(bean, i);
		} else if (paramType == float.class || paramType == Float.class) {
			Float f = new Float(value);
			m.invoke(bean, f);
		} else if (paramType == double.class || paramType == Double.class) {
			Double d = new Double(value);
			m.invoke(bean, d);
		} else {
			m.invoke(bean, value);
		}
	}

	private Method getSetter(@SuppressWarnings("rawtypes") Class beanClass,
			String property) throws Exception {
		StringBuilder sb = new StringBuilder("set");
		char c = property.charAt(0);
		if (c >= 'a' && c <= 'z')
			c -= 32;
		sb.append(c).append(property.substring(1));
		Method[] methods = beanClass.getMethods();

		log.debug("method name: " + sb);

		for (Method m : methods) {
			if (m.getName().equals(sb.toString()))
				return m;
		}
		throw new RuntimeException("No such property: " + property);
	}
}
