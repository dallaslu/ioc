package com.dallaslu.ioc;

public interface BeanContext {

	/**
	 * get bean instance by id
	 * @param id
	 * @return
	 */
	public Object getBean(String id);
}
