package com.dallaslu.ioc.test;

import com.dallaslu.ioc.BeanContext;
import com.dallaslu.ioc.BeanFactory;
import com.dallaslu.ioc.test.bean.User;

public class Tester {

	public static void main(String[] args) {
		BeanContext bc = BeanFactory.getInstance().parse(
				Tester.class.getResourceAsStream("/application-context.xml"));
		User user = (User) bc.getBean("user");

		System.out.println(user.getName() + " works in "
				+ user.getDepartment().getName());
	}

}
