package com.dallaslu.ioc.test.bean;

public class User {
	private final String name = "Jim Green";
	private Department department;
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public String getName() {
		return name;
	}
}
