package com.dallaslu.ioc.test.bean;

public class Department {
	private String name = "Develop Center";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
