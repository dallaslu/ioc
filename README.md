# ioc
a tiny implement of IOC

# Maven

    <dependency>
      <groupId>com.dallaslu</groupId>
      <artifactId>ioc</artifactId>
      <version>0.0.4</version>
    </dependency>

# How to
1. create a xml configuration file;
2. use `BeanFactory.getInstance().parse( configFilePath ).getBean( beanId )` to get bean.

# Resourses

1. test code: see [src/test](../../tree/master/src/test)
2. web utils: [ioc-web](https://github.com/dallaslu/ioc-web)